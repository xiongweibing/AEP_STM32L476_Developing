
/*                                                                             
 * Copyright(c) 2017 Communication Technologies Co., Ltd              
 * All rights reserved.                                                        
 *                                                                             
 * Company : Shanghai Research Institute of China Telecom Corporation
 * Department : IOT Terminal
 * Project : AEP
 * Title  : wifi esp8266.c                                                       
 * Date: 2017.12.11                                                         
 * Author:  wang-ty    17321294617                                                          
 * Revision:  1.1.1                                                              
 */                                                                            
                                                                               
/*****************************************************************************/
/*    INCLUDE FILE DECLARATIONS                                              */
/*****************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "stm32l4xx_hal.h"
#include "esp8266.h" 
#include "uart.h"
#include "main.h"
#include "stdint.h"


extern char USART_RX_BUF[Uart_Buf_Max];
/*******************************************************************/
char dtbuf[1024];



/*******************************************************************
* Name: nesp8266_at												   
* Input param: 
* Return-Value:
* Description: AT test
*******************************************************************/
 void esp8266_at(void);
{
	Send_Command(AT_CMD_AT, "OK", 2000, 1);
	HAL_Delay(1000);
}


/*******************************************************************
* Name: esp8266_reset											   
* Input param: 
* Return-Value:
* Description: Reset
*******************************************************************/
 void esp8266_reset(void);
{
	Send_Command(AT_CMD_RESET, "OK", 2000, 1);
	HAL_Delay(1000);
}


/*******************************************************************
* Name: esp8266_station											   
* Input param: 
* Return-Value:
* Description: Station mode
*******************************************************************/
 void esp8266_station(void);
{
	Send_Command(AT_CMD_MODE_STATION, "OK", 2000, 1);
	HAL_Delay(1000);
}


/*******************************************************************
* Name: esp8266_scan											   
* Input param: 
* Return-Value:
* Description: SEARCH WIFI
*******************************************************************/
 void esp8266_scan(void);
{
	Send_Command(AT_CMD_WIFI_SCAN, "OK", 2000, 1);
	HAL_Delay(1000);
}


/*******************************************************************
* Name: esp8266_connect											   
* Input param: 
* Return-Value:
* Description: CONNECT WITH SSID PASSWARD
*******************************************************************/
 void esp8266_connect(void);
{
	Send_Command(AT_CMD_WIFI_CONNECT, "OK", 2000, 1);
	HAL_Delay(1000);
}


/*******************************************************************
* Name: esp8266_disconnect											   
* Input param: 
* Return-Value:
* Description: DISCONNET WIFI
*******************************************************************/
 void esp8266_disconnect(void);
{
	Send_Command(AT_CMD_WIFI_DISCONNECT, "OK", 2000, 1);
	HAL_Delay(1000);
}


/*******************************************************************
* Name: esp8266_get_ip											   
* Input param: 
* Return-Value:
* Description: Local IP
*******************************************************************/
 void esp8266_get_ip(char *buf);
{
	Send_Command(AT_CMD_LOCAL_IP, "OK", 2000, 1);
	HAL_Delay(1000);
}


/*******************************************************************
* Name: esp8266_get_gmr											   
* Input param: 
* Return-Value:
* Description: MANUFACTUREER
*******************************************************************/
 void esp8266_get_gmr(char *buf);
{
	Send_Command(AT_CMD_MANUFACTUREER, "OK", 2000, 1);
	HAL_Delay(1000);
}


/*******************************************************************
* Name: esp8266_tcp_socket									   
* Input param: 
* Return-Value:
* Description: Create socket
*******************************************************************/
 void esp8266_tcp_socket(void);
{
	Send_Command(AT_CMD_CREAT_TCP_SOCKET, "OK", 2000, 1);
	HAL_Delay(1000);
}


/*******************************************************************
* Name: esp8266_set_singleway								   
* Input param: 
* Return-Value:
* Description: Single way
*******************************************************************/
 void esp8266_set_singleway(void);
{
	Send_Command(AT_CMD_SET_SINGLEWAY, "OK", 2000, 1);
	HAL_Delay(1000);
}


/*******************************************************************
* Name: esp8266_set_dtu								   
* Input param: 
* Return-Value:
* Description: set direct transmission
*******************************************************************/
 void esp8266_set_dtu(void);
{
	Send_Command(AT_CMD_SET_DTU, "OK", 2000, 1);
	HAL_Delay(1000);
}


/*******************************************************************
* Name: esp8266_set_send								   
* Input param: 
* Return-Value:
* Description: SEND MESSAGE AFTER THIS CMD
*******************************************************************/
 void esp8266_set_send(void);
{
	Send_Command(AT_CMD_SEND_MESSAGE, "OK", 2000, 1);
	HAL_Delay(1000);
}


/*******************************************************************
* Name: esp8266_set_sendquit								   
* Input param: 
* Return-Value:
* Description: quit direct transmission mode
*******************************************************************/
 void esp8266_set_sendquit(void);
{
	Send_Command(AT_CMD_SEND_QUIT, "OK", 2000, 1);
	HAL_Delay(1000);
}