#include <string.h>
#include "stm32l4xx_hal.h"

#include "uart.h"

UART_HandleTypeDef huart1;


extern char USART_RX_BUF[];


void CLR_Buf(void)                       
{
	memset(USART_RX_BUF, 0, Uart_Buf_Max);      
    point = 0;                    
}

unsigned int Send_Command(char * Command, char *Response, unsigned long Timeout, unsigned char Retry)
{
	unsigned char n;
	
	for (n = 0; n < Retry; n++)
	{
		HAL_UART_Transmit(&huart2, (uint8_t *)Command, (strlen(Command)), Timeout);
		HAL_Delay(1000);
	}
	CLR_Buf();
	return Uart_ERROR;
}

unsigned int Send_Command_To_Read(char * Command, char *ResponseVal, unsigned long Timeout, unsigned char Retry)
{
	unsigned char n;
	CLR_Buf();
	
	for (n = 0; n < Retry; n++)
	{
		HAL_UART_Transmit(&huart2, (uint8_t *)Command, (strlen(Command)), Timeout);
	}
	//CLR_Buf();
	return Uart_ERROR;
}

