
/*                                                                             
 * Copyright(c) 2017 Communication Technologies Co., Ltd              
 * All rights reserved.                                                        
 *                                                                             
 * Company : Shanghai Research Institute of China Telecom Corporation
 * Department : IOT Terminal
 * Project : AEP
 * Title  : wifi esp8266.h                                                       
 * Date: 2017.12.11                                                         
 * Author:  wang-ty    17321294617                                                          
 * Revision:  1.1.1                                                              
 */  

#ifndef _ESP8266_H_
#define _ESP8266_H_


//Basic CMD
#define            AT_CMD_AT                       "AT\r\n"                         //AT test
#define            AT_CMD_RESET                    "AT+RST\r\n"                     //Reset
#define            AT_CMD_MANUFACTUREER            "AT+GMR\r\n"                     //GMR

//WIFI CMD
#define            AT_CMD_MODE_STATION             "AT+CWMODE=1\r\n"                        //Station mode
#define            AT_CMD_MODE_AP                  "AT+CWMODE=2\r\n"                        //AP mode
#define            AT_CMD_MODE_APSTATION           "AT+CWMODE=3\r\n"                        //Station+AP mode
#define		       AT_CMD_WIFI_CONNECT		       "AT+CWJAP=\"HUAWEI-GPTAR7\",\"1qaz2wsx\"\r\n"	    //SSID PASSWARD
#define            AT_CMD_WIFI_SCAN                "AT+CWLAP\r\n"                           //SEARCH WIFI
#define            AT_CMD_WIFI_DISCONNECT          "AT+CWQAP\r\n"                           //DISCONNET WIFI
#define            AT_CMD_LOCAL_IP                 "AT+CIFSR\r\n"                           //Local IP

//TCP message
#define            AT_CMD_CREAT_TCP_SOCKET         "AT+CIPSTART=\"TCP\",\"192.168.157.232\",1883\r\n"	 //Create socket
#define            AT_CMD_SET_SINGLEWAY            "AT+CIPMUX=0\r\n"                                   //Single way
#define            AT_CMD_SET_DTU                  "AT+CIPMODE=1\r\n"			                          	 //	 direct transmission
#define            AT_CMD_SEND_MESSAGE             "AT+CIPSEND\r\n"	     		                           //SEND MESSAGE AFTER THIS CMD
#define            AT_CMD_SEND_QUIT                "+++"	    	                                	 //QUIT SENDING
#define            AT_CMD_CLOSE_TCP                "AT+CIPCLOSE\r\n"									//close TCP quickly


/*recv from device*/
#define 	      	AT_CMD_OK		   		          	"OK"
#define 	      	AT_CMD_ERROR			          	"ERROR"
#define		      	AT_CMD_INVALID_CMD	              	"Invalid AT Command Format"
			   

/*ip and port*/
//#define               AT_CMD_IP					"\"117.60.157.30\""
//#define		      	AT_CMD_PORT		 	    	"\"5683\""					
#define                 AT_CMD_IP					"\"101.89.150.71\""
#define		        	AT_CMD_PORT		            "\"1883\""



 void esp8266_at(void);
 void esp8266_reset(void);
 
 void esp8266_station(void);
 void esp8266_scan(void);
 void esp8266_connect(void);
 void esp8266_disconnect(void);
 void esp8266_get_ip(char *ip);
 void esp8266_get_gmr(char *gmr);
 
 void esp8266_tcp_socket(void);
 void esp8266_set_singleway(void);
 void esp8266_set_dtu(void);
 void esp8266_set_send(void);
 void esp8266_set_sendquit(void);
 void esp8266_close_tcp(void);


 //void esp8266_send(char *data);

 
/* typedef enum NB_return_e {
	NB_STATUS_SUCCESS,
	NB_STATUS_FAILED,
	NB_STATUS_INVALID_PARMS,
	NB_STATUS_TIMEOUT
} NB_return_t;
*/
#endif
