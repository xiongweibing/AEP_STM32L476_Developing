#ifndef _UART_H_
#define _UART_H_

#define Uart_Buf_Max 	512
#define Uart_OK			1 
#define Uart_ERROR		2 
#define Wifi_OK         3
#define Wifi_ERROR      4

void CLR_Buf(void);
unsigned int Send_Command(char * Command, char *Response, unsigned long Timeout, unsigned char Retry);
unsigned int Wifi_Send(char * data, char *Response, unsigned long Timeout, unsigned char Retry);
#endif

