/*                                                                             
 * Copyright(c) 2017 Communication Technologies Co., Ltd              
 * All rights reserved.                                                        
 *                                                                             
 * Company : Shanghai Research Institute of China Telecom Corporation
 * Department : IOT Terminal
 * Project : AEP
 * Title  : uart.c                                                      
 * Date: 2017.12.21                                                         
 * Author:  Terminal Group                                                          
 * Revision:  1.1.1                                                              
 */  
 
#include <string.h>
#include "stm32l4xx_hal.h"

#include "uart.h"

extern UART_HandleTypeDef huart1;
extern int getdata_flag;

char USART_RX_BUF[Uart_Buf_Max];
uint16_t point = 0;

void CLR_Buf(void)                       
{
	memset(USART_RX_BUF, 0, Uart_Buf_Max);      
    point = 0;      
	getdata_flag = 0;	
}

unsigned int Send_Command(char * Command, char *Response, unsigned long Timeout, unsigned char Retry)
{
	unsigned char n;
	
	for (n = 0; n < Retry; n++)
	{
		HAL_UART_Transmit(&huart1, (uint8_t *)Command, (strlen(Command)), Timeout);
		HAL_Delay(1000);
	}
	//CLR_Buf();
	return Uart_ERROR;
}

unsigned int Wifi_Send(char * data, char *Response, unsigned long Timeout, unsigned char Retry)
{
	unsigned char n;
	
	for (n = 0; n < Retry; n++)
	{
		HAL_UART_Transmit(&huart1, (uint8_t *)data, (strlen(data)), Timeout);
		HAL_Delay(1000);
	}
	//CLR_Buf();
	return Wifi_ERROR;
}
